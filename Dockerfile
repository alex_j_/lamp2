FROM ubuntu:trusty

# export DEBIAN_FRONTEND=noninteractive
ENV DEBIAN_FRONTEND noninteractive

# update the system
RUN apt-get -y update

# Install packages	
RUN apt-get -y install supervisor git apache2 libapache2-mod-php5 mysql-server php5-mysql && \
	pwgen php-apc php5-mcrypt echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Download files
RUN git clone https://alex_j_@bitbucket.org/alex_j_/lamp2.git /files
RUN mkdir -p /files/app && rm -fr /var/www/html && ln -s /files/app /var/www/html

# Add image configuration and scripts
RUN mv /files/conf/start-apache2.sh /start-apache2.sh && \
	mv /files/conf/start-mysqld.sh /start-mysqld.sh && \
	mv /files/conf/run.sh /run.sh && \
	mv /files/conf/create_mysql_admin_user.sh /create_mysql_admin_user.sh && \
	chmod 755 /*.sh  && \
	mv /files/conf/my.cnf /etc/mysql/conf.d/my.cnf  && \
	mv /files/conf/supervisord-apache2.conf /etc/supervisor/conf.d/supervisord-apache2.conf && \
	mv /files/conf/supervisord-mysqld.conf /etc/supervisor/conf.d/supervisord-mysqld.conf

# Remove pre-installed database
RUN rm -rf /var/lib/mysql/*

# config to enable .htaccess
RUN mv /files/conf/apache_default /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite

#Enviornment variables to configure php
ENV PHP_UPLOAD_MAX_FILESIZE 10M
ENV PHP_POST_MAX_SIZE 10M

# Add volumes for MySQL 
VOLUME  ["/etc/mysql", "/var/lib/mysql" ]

EXPOSE 80 3306
CMD ["/run.sh"]
